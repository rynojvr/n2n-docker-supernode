FROM registry.gitlab.com/rynojvr/n2n-docker:latest as repo

FROM debian:stretch
COPY --from=repo /usr/sbin/supernode /usr/sbin/
COPY supernode.conf.sample /n2n/supernode.conf

ENTRYPOINT ["/usr/sbin/supernode", "/n2n/supernode.conf"]